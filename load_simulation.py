import matplotlib.pyplot as plt
import numpy as np


class SystemPowerAnalyzer:

    def __init__(self, systems, cycles=1):
        self.systems = systems
        self.cycles = cycles
        self.granularity = self.calculate_granularity()
        self.time_range = self.generate_time_range()

        # Apply safety_factors
        for sn in self.systems.keys():
            try:
                self.systems[sn]['active_power'] = self.systems[sn]['active_power'] * (1+self.systems[sn]['safety_factor']/100)  # noqa: E501
                self.systems[sn]['idle_power'] = self.systems[sn]['idle_power'] * (1+self.systems[sn]['safety_factor']/100)  # noqa: E501
            except KeyError:
                continue

    def total_power_at_time(self, time):
        total_power = 0
        for system_name, system_params in self.systems.items():
            cycle_time = system_params['active_time'] + system_params[
                'idle_time']
            time_in_cycle = (time -
                             system_params['activation_delay']) % cycle_time

            if time_in_cycle < system_params[
                    'active_time'] and time >= system_params[
                        'activation_delay']:
                total_power += system_params['active_power'] / system_params[
                    'power_channel_efficiency']
            else:
                total_power += system_params['idle_power'] / system_params[
                    'power_channel_efficiency']
        return total_power

    def calculate_maximum_load(self):
        max_load = sum([
            system_params['active_power']
            for system_params in self.systems.values()
        ])
        return max_load

    def calculate_average_power(self, start_time, end_time):
        total_power = 0
        num_points = 0

        time_values = np.arange(start_time, end_time, self.granularity)
        for t in time_values:
            if start_time <= t <= end_time:
                total_power += self.total_power_at_time(t)
                num_points += 1

        if num_points > 0:
            average_power = total_power / num_points
        else:
            average_power = 0

        return average_power

    def plot_systems_loads(self, name=''):
        time_values = np.arange(0, max(self.time_range), self.granularity)
        total_power_values = [self.total_power_at_time(t) for t in time_values]

        plt.figure(figsize=(10, 6), num=f'{name} Systems loads')
        plt.subplot(2, 1, 1)
        plt.axhline(y=self.calculate_maximum_load(),
                    color='red',
                    linestyle='--',
                    label='Maximum Possible Load')
        plt.axhline(y=max(total_power_values),
                    color='orange',
                    linestyle='--',
                    label='Peak Power')
        plt.plot(time_values,
                 total_power_values,
                 label='Total Power (mW)',
                 drawstyle='steps-post')
        plt.xlabel('Time (s)')
        plt.ylabel('Total Power (mW)')
        plt.legend()
        plt.grid(True)

        system_states = []

        for t in time_values:
            state = []
            for system_name, system_params in self.systems.items():
                cycle_time = system_params['active_time'] + system_params[
                    'idle_time']
                time_in_cycle = (
                    t - system_params['activation_delay']) % cycle_time

                if t < system_params['activation_delay']:
                    state.append('Idle')
                elif time_in_cycle < system_params['active_time']:
                    state.append('Active')
                else:
                    if time_in_cycle < cycle_time:
                        state.append('Idle')
                    else:
                        state.append('Active')

            system_states.append(state)

        plt.subplot(2, 1, 2)
        colors = {'Active': 'seagreen', 'Idle': 'lightgray'}
        for i, system_name in enumerate(self.systems.keys()):
            system_state = [state[i] for state in system_states]
            plt.broken_barh(
                [(time_values[i], self.granularity)
                 for i, s in enumerate(system_state) if s == 'Active'],
                (i - 0.4, 0.8),
                facecolors=colors['Active'])
            plt.broken_barh(
                [(time_values[i], self.granularity)
                 for i, s in enumerate(system_state) if s == 'Idle'],
                (i - 0.4, 0.8),
                facecolors=colors['Idle'])

        plt.xlabel('Time (s)')
        system_names = []
        for system_id in self.systems.keys():
            system_names.append(self.systems[system_id].get('name')
                                or system_id)
        plt.yticks(np.arange(len(self.systems)), system_names)
        plt.grid(True)

        plt.tight_layout()
        return plt

    def calculate_granularity(self):
        active_times = [
            system_params['active_time']
            for system_params in self.systems.values()
            if system_params['active_time'] > 0
        ]
        idle_times = [
            system_params['idle_time']
            for system_params in self.systems.values()
            if system_params['idle_time'] > 0
        ]
        activation_delays = [
            system_params['activation_delay']
            for system_params in self.systems.values()
            if system_params['activation_delay'] > 0
        ]
        smallest_time_step = min(active_times + idle_times + activation_delays)
        print(f'Timestep: {smallest_time_step}')
        return smallest_time_step

    def generate_time_range(self):
        # Find the max cycle time considering activation_delay
        max_cycle_time = max(system_params['active_time'] +
                             system_params['idle_time'] +
                             system_params['activation_delay']
                             for system_params in self.systems.values())
        print(f'Max cycle time: {max_cycle_time}')

        time_range = np.arange(0, max_cycle_time * self.cycles,
                               self.granularity)
        return time_range


# Example usage:
if __name__ == '__main__':

    systems_dict = {
        'ADCS': {
            'active_power': 100,
            'idle_power': 10,
            'active_time': 1,
            'idle_time': 10,
            'activation_delay': 5
        },
        'GPS': {
            'active_power': 150,
            'idle_power': 20,
            'active_time': 10,
            'idle_time': 50,
            'activation_delay': 5
        },
        'Payload': {
            'active_power': 200,
            'idle_power': 0,
            'active_time': 5,
            'idle_time': 5,
            'activation_delay': 1
        },
        'Payload2': {
            'active_power': 200,
            'idle_power': 0,
            'active_time': 1,
            'idle_time': 0,
            'activation_delay': 1
        }
    }

    analyzer = SystemPowerAnalyzer(systems_dict)

    # Calculate and print the maximum possible load
    max_load = analyzer.calculate_maximum_load()
    print(f'Maximum Possible Load: {max_load} mW')

    # Calculate and print the average power consumption
    # for a given time frame (t=30 to t=90 seconds)
    average_power = analyzer.calculate_average_power(start_time=0, end_time=10)
    print(f'Average Power Consumption: {average_power} mW')

    # Plot the total power and Gantt chart
    analyzer.plot_total_power()
    analyzer.plot_gantt_chart()
