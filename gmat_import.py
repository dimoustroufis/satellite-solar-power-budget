import argparse
import os
from datetime import datetime, timedelta


def date2epoch(date_string, date_format):
    """
    Convert a date string in UTCModJulian or UTCGregorian format
    to decimal days since December 31, 1949.

    Args:
        date_string (str): The input date string.
        date_format (str): The format of the input date string.
            Supported formats are 'UTCModJulian' and 'UTCGregorian'.

    Returns:
        float: The number of decimal days since December 31, 1949.

    Raises:
        ValueError: If an invalid date format is provided.
    """
    reference_date = datetime(1949, 12, 31, 0, 0)

    if date_format == 'UTCModJulian':
        gmat_reference_epoch = datetime(1941, 1, 5, 12, 0, 0)
        input_date = gmat_reference_epoch + timedelta(days=float(date_string))
    elif date_format == 'UTCGregorian':
        input_date = datetime.strptime(date_string, '%d %b %Y %H:%M:%S.%f')
    else:
        raise ValueError('Invalid date format')

    days = (input_date - reference_date).total_seconds() / 86400
    return days


def parse_mission_file(filename, spacecraft_name):
    # Open the GMAT mission file
    try:
        with open(os.path.expanduser(filename), 'r') as f:
            mission_lines = f.readlines()
    except Exception as e:
        print(f'Error: Failed to open GMAT mission file "{filename}"')
        print(e)
        return None

    # Get the spacecraft object from the mission file
    spacecraft_start_index = None
    for i, line in enumerate(mission_lines):
        if f'Create Spacecraft {spacecraft_name}' in line:
            spacecraft_start_index = i
            break
    if spacecraft_start_index is None:
        print(
            f'Error: Failed to get spacecraft object "{spacecraft_name}"\
                 from GMAT mission file'
        )
        return None
    spacecraft_lines = mission_lines[spacecraft_start_index:]

    # Get the spacecraft's Keplerian orbital elements
    keplerian_elements = {}
    for line in spacecraft_lines:
        line = line.strip().rstrip(
            ';')  # Strip leading and trailing whitespace and semicolons
        if 'SMA =' in line:
            keplerian_elements['SMA'] = float(
                line.split('=')[1].strip().split(' ')[0])
        elif 'ECC =' in line:
            keplerian_elements['eccentricity'] = float(
                line.split('=')[1].strip())
        elif 'INC =' in line:
            keplerian_elements['inclination'] = float(
                line.split('=')[1].strip())
        elif 'RAAN =' in line:
            keplerian_elements['RAAN'] = float(line.split('=')[1].strip())
        elif 'AOP =' in line:
            keplerian_elements['AOP'] = float(line.split('=')[1].strip())
        elif 'TA =' in line:
            keplerian_elements['true_anomaly'] = float(
                line.split('=')[1].strip())
        elif 'DateFormat =' in line:
            date_format = line.split('=')[1].strip()
        elif 'Epoch =' in line:
            gmat_epoch = line.split('=')[1].strip().strip('"')

        # Stop parsing after the first spacecraft
        if keplerian_elements and line.strip().startswith('Create Spacecraft'):
            break
    print(f'Calculating epoch for {gmat_epoch} {date_format}')
    keplerian_elements['epoch'] = date2epoch(gmat_epoch, date_format)
    # Check if all Keplerian elements were found
    if len(keplerian_elements) != 7:
        print(
            f'Error: Failed to get spacecraft "{spacecraft_name}"\
                " Keplerian orbital elements'
        )
        return None

    return keplerian_elements


def main():
    # Parse command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('filename', help='GMAT mission file name')
    parser.add_argument('spacecraft_name',
                        help='Spacecraft name in GMAT mission file')
    args = parser.parse_args()

    # Check if the file exists
    if not os.path.isfile(args.filename):
        print(f'Error: GMAT mission file "{args.filename}" not found')
        return

    # Parse the mission file
    keplerian_elements = parse_mission_file(args.filename,
                                            args.spacecraft_name)

    # Print the Keplerian orbital elements
    for key, value in keplerian_elements.items():
        print(f'{key.capitalize()}: {value}')


if __name__ == '__main__':
    main()
