""" This script get input from user to calculate the mean power production of
a satellite in orbit.
Author: Yannis Koveos (https://gitlab.com/ykoveos)
License: GNU GENERAL PUBLIC LICENSE Version 3
Requirements:
* SGP4: https://pypi.python.org/pypi/sgp4/
* PyKDL: https://pypi.org/project/PyKDL/
* numpy: version 1.16.2
* matplotlib : version 2.2.2
"""

from math import ceil, floor, pi

import numpy as np
import quaternion
# import PyKDL
from tqdm import tqdm
import pandas as pd


def sunpos_ECI(JD):
    """Calculate sun position vector.

    @package This algorithm is from "Fundamentals of Astrodynamics and
    Applications", David A. Vallado, 1997, McGraw-Hill, Alg. 18, pg.183.
    For quick debug use Ex. 3-8, or use an online sun position calculator
    @param Julian Date
    @return d,r Earth Sun distance (AU), Sun position vector in ECI (AU)
    """
    T_UT1 = (JD - 2451545.0) / 36525
    l_m = 280.4606184 + 36000.77005361 * T_UT1  # Mean longitude of the sun
    l_m = np.fmod(l_m, 360.0)
    if l_m < 0:
        l_m = l_m + 360.0
    T_TDB = T_UT1

    man_sun = 357.5277233 + 35999.05034 * T_TDB  # Mean anomaly of Sun
    man_sun = np.fmod(man_sun, 360.0)
    if man_sun < 0:
        man_sun = man_sun + 360.0
    man_sun = man_sun * np.pi / 180.0  # In RADIANS

    # Ecliptic longitude of Sun, in RADIANS
    l_ecl = (l_m + 1.914666471 * np.sin(man_sun) +
             0.019994643 * np.sin(2 * man_sun)) * np.pi / 180.0
    # Distance to Sun in AUs
    d = 1.000140612 - 0.016708617 * np.cos(man_sun)
    -0.000139589 * np.cos(2 * man_sun)
    eps = (23.439291 - 0.0130042 * T_TDB) * np.pi / 180.0  # In RADIANS
    r = (d * np.cos(l_ecl), d * np.cos(eps) * np.sin(l_ecl),
         d * np.sin(eps) * np.sin(l_ecl))
    return d, r


def get_orbit_period(satellite):
    """Determine orbit period in munutes from TLE."""
    return (2 * pi) / satellite.no_kozai


def get_sat_frame_angV(satellite):
    """Return angular velocity of Y axis when X+ is
    pointing to the velocity vector"""
    return satellite.no_kozai


def power_calculation(satellite,
                      JD_ini,
                      orbits,
                      ang_v,
                      pose,
                      pv_eff,
                      pv_count,
                      pv_area,
                      timestep=1,
                      show_progress=False,
                      si=1.361):
    """
    Calculate accumulated power for each side of the satellite.

        Parameters:
            satellite (object): SGP4 Satrec
            JD_ini: Julian day for simulation start
            orbits (int): Number of orbits to simulate
            ang_v (list): Angular velocity for XYZ
            pose (list): Initial pose for XYZ
            pv_eff (list): PV Efficiency
            pv_count (list): Number of PVs per side
            pv_area (list): Area of each PV element
            timestep (int): Simulation time step in seconds
            show_progress (bool): Display progress bar
            si (float): Solar irradiance (kW/m^2) in a specific orbit
        Returns:
            mean_power_coeff (list): Mean power coefficient for each axis
            mean_power (list): Mean power for each axis
            side_power_values (list): Power values for each axis
            orbit_side_mean_power (list): Mean power per orbit for each axis
    """
    orbit_period = get_orbit_period(satellite) * (60 / timestep)
    total_time = ceil(orbits * orbit_period)  # in min

    # Mean power of each side in mW
    mean_power = np.array([0, 0, 0, 0, 0, 0])
    # Earth radius in km
    r_earth = 6371.0
    # Power coefficient of each side
    fxp = np.zeros(total_time)
    fxm = np.zeros(total_time)
    fyp = np.zeros(total_time)
    fym = np.zeros(total_time)
    fzp = np.zeros(total_time)
    fzm = np.zeros(total_time)
    # Orbit and daily means
    orbit_side_mean_coeff = []
    # Emulate the orbit
    if show_progress:
        pbar = tqdm(
            total=total_time,
            # desc=TLE[0],
            bar_format='{l_bar}{bar} [ time left: {remaining} ]',
            initial=0)
    eclipse = []
    eclipse_pc = []  # eclipse percentage per orbit

    # Load attitude data timeseries - [x,y,z,w] unit quaternions from ECI to body frame 
    # colnames = ["q1","q2","q3","q0"]
    att_path = "qbn.42"
    qbf_eci = pd.read_csv(att_path,delimiter = ' ',engine = "pyarrow",header=None)
    qbf_eci = np.array(qbf_eci)

    # Load position data timeseries - [x,y,z] in ECI
    # colnames = ["r0","r1","r2"]
    pos_path = "PosN.42"
    r = pd.read_csv(pos_path,delimiter = ' ',engine = "pyarrow",header=None)
    r = np.array(r)
    r = r * 1e-3 # convert to km

    # Load position data timeseries - [x,y,z] in ECI
    # colnames = ["v0","v1","v2"]
    vel_path = "VelN.42"
    v = pd.read_csv(vel_path,delimiter = ' ',engine = "pyarrow",header=None)
    v = np.array(v)
    v = v * 1e-3 # convert to km/s

    for curr_time in range(0, total_time):
        print(curr_time)
        # Time in Julian days, curr_time in minutes
        JD = JD_ini + curr_time / (1440.0 * 60 / timestep)
        # d_sun Earth Sun distance (AU) , Sun position vector r_sun in ECI (AU)
        d_sun, r_sun = sunpos_ECI(JD)
        r_sun = np.array([r_sun[0], r_sun[1], r_sun[2]])
        # r_sun = PyKDL.Vector(r_sun[0], r_sun[1], r_sun[2])
        r_sun = r_sun / np.linalg.norm(r_sun)

        # Rotation from body frame to ECI
        # Using the conjugate quaternion and a scalar last convention 
        q = np.quaternion(+qbf_eci[curr_time,3],-qbf_eci[curr_time,0],-qbf_eci[curr_time,1],-qbf_eci[curr_time,2])
        # q = np.quaternion(qbf_eci[curr_time,3],qbf_eci[curr_time,0],qbf_eci[curr_time,1],qbf_eci[curr_time,2])
        Rbf_eci = quaternion.as_rotation_matrix(q)

        # Body vector in body frame
        Xbf = np.array([1.0, 0.0, 0.0])
        Ybf = np.array([0.0, 1.0, 0.0])
        Zbf = np.array([0.0, 0.0, 1.0])

        # Body vector in ECI
        Xbf_eci = np.matmul(Rbf_eci,np.transpose([Xbf])).flatten()
        Ybf_eci = np.matmul(Rbf_eci,np.transpose([Ybf])).flatten()
        Zbf_eci = np.matmul(Rbf_eci,np.transpose([Zbf])).flatten()

        # Satellite vector in ECI to check if the sun is behind the earth
        r_sat = np.array([-r[curr_time,0], -r[curr_time,1], -r[curr_time,2]])

        # Check if the sun is behind the earth (Eclipse)
        tmp = np.dot(r_sat, r_sun)
        if ((r_earth > np.linalg.norm(r_sat - tmp * r_sun)) and (tmp > 0)):
            r_sun = 0.0 * r_sun
            eclipse.append(True)
        else:
            eclipse.append(False)
        # Calculate the projection of sun vector in each side of satellite,
        # power efficiency
        # X axis
        tmp = np.dot(Xbf_eci, r_sun)
        if tmp >= 0.0:
            fxp[curr_time] = tmp
        else:
            fxm[curr_time] = -tmp
        # Y axis
        tmp = np.dot(Ybf_eci, r_sun)
        if tmp >= 0.0:
            fyp[curr_time] = tmp
        else:
            fym[curr_time] = -tmp
        # Z axis
        tmp = np.dot(Zbf_eci, r_sun)
        if tmp >= 0.0:
            fzp[curr_time] = tmp
        else:
            fzm[curr_time] = -tmp

        # Per orbit calculations
        if (curr_time > 0) and (((curr_time) % floor(orbit_period)) == 0):
            # Store orbit mean pwr
            orbit_side_mean_coeff.append(
                calc_avg_power_coeff(
                    (fxp[(curr_time - floor(orbit_period) + 1):curr_time],
                     fxm[(curr_time - floor(orbit_period) + 1):curr_time],
                     fyp[(curr_time - floor(orbit_period) + 1):curr_time],
                     fym[(curr_time - floor(orbit_period) + 1):curr_time],
                     fzp[(curr_time - floor(orbit_period) + 1):curr_time],
                     fzm[(curr_time - floor(orbit_period) + 1):curr_time]),
                    floor(orbit_period)))
            # Get eclipse time
            eclipse_pc.append(sum(eclipse) / orbit_period)
            eclipse = []

        if show_progress:
            pbar.update(1)
    p_eff_values = (fxp, fxm, fyp, fym, fzp, fzm)
    mean_power_coeff = calc_avg_power_coeff(p_eff_values, total_time)
    # PV mean power production
    mean_power = pv_area * pv_eff * pv_count * mean_power_coeff * si
    # Plot the power coefficient
    side_power_values = []
    orbit_side_mean_power = []
    for axis, side in enumerate(p_eff_values):
        side_power_values.append(side * si * pv_area[axis] * pv_eff[axis] *
                                 pv_count[axis])
    for axis, side in enumerate(np.transpose(orbit_side_mean_coeff)):
        orbit_side_mean_power.append(side * si * pv_area[axis] * pv_eff[axis] *
                                     pv_count[axis])
    return (mean_power_coeff, mean_power, side_power_values,
            orbit_side_mean_power, eclipse_pc)


def calc_avg_power_coeff(pwr_eff, total_time):
    """Calculate average power coefficient.

    Calculate the integral of power coefficient to get the energy coefficient
    and then the division with total time to get the average power coefficient
    """
    mean_power_coeff = []
    for axis_pwr in pwr_eff:
        mean_power_coeff.append(np.trapz(axis_pwr) / total_time)
    return mean_power_coeff
