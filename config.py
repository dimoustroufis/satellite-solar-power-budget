import os.path
import sys
from datetime import datetime
from pathlib import Path

import numpy as np
import yaml
from sgp4.api import Satrec
from sgp4.ext import jday

from gmat_import import parse_mission_file
from keplerians import create_satellite

axis_labels = ['X+', 'X-', 'Y+', 'Y-', 'Z+', 'Z-']


def read_config(conf_filename):
    """Read parameters configuration file."""
    with open(conf_filename, mode='r', encoding='utf-8') as file:
        try:
            sim_params = {}
            sim_params['spacecrafts'] = []
            sim_params['skip_plot_axis'] = []
            parameters = yaml.load(file, Loader=yaml.FullLoader)
            # parse TLEs
            try:
                for name, spacecraft in parameters['TLEs'].items():
                    sat = Satrec.twoline2rv(spacecraft['TLE'][0],
                                            spacecraft['TLE'][1])
                    sim_params['spacecrafts'].append((name, sat))
            except (AttributeError, TypeError, KeyError):
                print('No TLE found')
            except Exception as e:
                print(e)
                exit()
            # Parse Keplerians
            try:
                for name, spacecraft in parameters['Keplerians'].items():
                    epoch = (
                        datetime.strptime(spacecraft['epoch'],
                                          '%d %b %Y %H:%M:%S.%f') -
                        datetime(1949, 12, 31, 0, 0)).total_seconds() / 86400
                    sat = create_satellite(
                        99997, epoch, spacecraft['semi_major_axis'],
                        spacecraft['eccentricity'], spacecraft['inclination'],
                        spacecraft['raan'],
                        spacecraft['argument_of_periapsis'],
                        spacecraft['true_anomaly'], spacecraft['bstar'])
                    sim_params['spacecrafts'].append((name, sat))

            except (AttributeError, TypeError, KeyError):
                print('No Keplerian elements found')
            except Exception as e:
                print(e)
                exit()
            # Parse GMAT
            try:
                for name in parameters['GMAT']['Spacecrafts']:
                    keplerian_elements = parse_mission_file(
                        parameters['GMAT']['File'], name)
                    # print(name, keplerian_elements)
                    sat = create_satellite(99997, keplerian_elements['epoch'],
                                           keplerian_elements['SMA'],
                                           keplerian_elements['eccentricity'],
                                           keplerian_elements['inclination'],
                                           keplerian_elements['RAAN'],
                                           keplerian_elements['AOP'],
                                           keplerian_elements['true_anomaly'],
                                           0.0)
                    sim_params['spacecrafts'].append((name, sat))
            except (AttributeError, TypeError, KeyError):
                print('No GMAT spacecraft found')
            except Exception as e:
                print(e)
                exit()
            # Show TLEs

            if len(sim_params['spacecrafts']) == 0:
                exit()
            # Initialize angular velocities in rad/min [X, Y, Z]
            sim_params['ang_v'] = np.array([
                parameters['Spacecraft']['ang_v']['X'],
                parameters['Spacecraft']['ang_v']['Y'],
                parameters['Spacecraft']['ang_v']['Z']
            ])
            # Initial angle conditions in deg [X, Y, Z]
            sim_params['pose0'] = np.deg2rad([
                parameters['Spacecraft']['pose0']['X'],
                parameters['Spacecraft']['pose0']['Y'],
                parameters['Spacecraft']['pose0']['Z']
            ])
            # PV efficient in each side [X+, X-, Y+, Y-, Z+, Z-]
            try:
                sim_params['pv_eff'] = np.array([
                    parameters['SolarCells']['efficiency']['X+'],
                    parameters['SolarCells']['efficiency']['X-'],
                    parameters['SolarCells']['efficiency']['Y+'],
                    parameters['SolarCells']['efficiency']['Y-'],
                    parameters['SolarCells']['efficiency']['Z+'],
                    parameters['SolarCells']['efficiency']['Z-']
                ])
            except KeyError:
                sim_params['pv_eff'] = None
            # Number of PV in each side [X+, X-, Y+, Y-, Z+, Z-]
            sim_params['pv_count'] = np.array([
                parameters['SolarCells']['count']['X+'],
                parameters['SolarCells']['count']['X-'],
                parameters['SolarCells']['count']['Y+'],
                parameters['SolarCells']['count']['Y-'],
                parameters['SolarCells']['count']['Z+'],
                parameters['SolarCells']['count']['Z-']
            ])
            # Active area of each PV in mm^2, [X+, X-, Y+, Y-, Z+, Z-]
            try:
                sim_params['pv_area'] = np.array([
                    parameters['SolarCells']['area']['X+'],
                    parameters['SolarCells']['area']['X-'],
                    parameters['SolarCells']['area']['Y+'],
                    parameters['SolarCells']['area']['Y-'],
                    parameters['SolarCells']['area']['Z+'],
                    parameters['SolarCells']['area']['Z-']
                ])
            except KeyError:
                sim_params['pv_area'] = None
            # Power of each PV in W, [X+, X-, Y+, Y-, Z+, Z-]
            try:
                sim_params['pv_pwr'] = np.array([
                    parameters['SolarCells']['power']['X+'],
                    parameters['SolarCells']['power']['X-'],
                    parameters['SolarCells']['power']['Y+'],
                    parameters['SolarCells']['power']['Y-'],
                    parameters['SolarCells']['power']['Z+'],
                    parameters['SolarCells']['power']['Z-']
                ])
            except KeyError:
                sim_params['pv_pwr'] = None
            # Set the start day for simulation, Julian day
            sim_params['JD_ini'] = jday(
                parameters['Sim']['JD_start']['year'],
                parameters['Sim']['JD_start']['month'],
                parameters['Sim']['JD_start']['day'],
                parameters['Sim']['JD_start']['hour'],
                parameters['Sim']['JD_start']['min'],
                parameters['Sim']['JD_start']['second'])
            sim_params['orbit_count'] = parameters['Sim']['orbits']
            try:
                sim_params['duration'] = parameters['Sim']['duration']
            except KeyError:
                sim_params['duration'] = None
            # Plot axis to skip
            for axis, skip in enumerate(parameters['Sim']['skip_plot_axis']):
                if skip or parameters['SolarCells']['count'][
                        axis_labels[axis]] == 0:
                    sim_params['skip_plot_axis'].append(axis)
            try:
                sim_params['output_path'] = os.path.expanduser(
                    parameters['Sim']['output_path'])
                print('Plots output path: ', sim_params['output_path'])
                Path(sim_params['output_path']).mkdir(parents=True,
                                                      exist_ok=True)
            except KeyError:
                sim_params['output_path'] = False
            sim_params['timestep'] = parameters['Sim']['timestep']
            try:
                sim_params['SolarLockY'] = parameters['Spacecraft']['ang_v'][
                    'SolarLockY']
            except KeyError:
                sim_params['SolarLockY'] = False
            # Validate Systems loads
            for name, params in parameters['Spacecraft']['loads'].items():
                if 'activation_delay' not in params:
                    parameters['Spacecraft']['loads'][name][
                        'activation_delay'] = 0
                if 'idle_time' not in params:
                    parameters['Spacecraft']['loads'][name][
                        'idle_time'] = params['interval'] - params[
                            'active_time']
                parameters['Spacecraft']['loads'][name].pop('interval')

        except yaml.YAMLError as e:
            print('Error reading parameters', e)
            sys.exit(1)
        return sim_params, parameters
