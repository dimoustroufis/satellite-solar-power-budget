from math import atan2, cos, radians, sin, sqrt

from sgp4 import exporter
from sgp4.api import WGS72, Satrec


def calculate_mean_anomaly(true_anomaly, eccentricity):
    """
    Calculates the mean anomaly given the true anomaly and eccentricity.

    Args:
        true_anomaly (float): True anomaly in radians.
        eccentricity (float): Eccentricity of the orbit.

    Returns:
        float: Mean anomaly in radians.
    """
    eccentric_anomaly = 2 * atan2(
        sqrt(1 - eccentricity) * sin(true_anomaly / 2),
        sqrt(1 + eccentricity) * cos(true_anomaly / 2))
    # eccentric_anomaly = atan2((1 - eccentricity**2) *
    # sin(true_anomaly), eccentricity + cos(true_anomaly))
    mean_anomaly = eccentric_anomaly - eccentricity * sin(eccentric_anomaly)

    return mean_anomaly


def calculate_mean_motion(mean_anomaly, semimajor_axis):
    """
    Calculates the mean motion given the mean anomaly and semimajor axis.

    Args:
        mean_anomaly (float): Mean anomaly in radians.
        semimajor_axis (float): Semimajor axis of the orbit.

    Returns:
        float: Mean motion in radians per second.
    """
    # Gravitational constant in m^3/(kg*s^2)
    gravitational_constant = 6.67430e-11
    earth_mass = 5.97219e24  # Mass of the Earth in kg

    mean_motion = sqrt((gravitational_constant * earth_mass) /
                       ((semimajor_axis * 1000)**3)) * 60

    return mean_motion


def create_satellite(satellite_number,
                     epoch,
                     semi_major_axis,
                     eccentricity,
                     inclination,
                     raan,
                     argument_of_periapsis,
                     true_anomaly,
                     bstar=0):
    """
    Calculates TLE from keplerian elements

    Args:
        satellite_number (str): Satellite number
        epoch (float): days since 1949 December 31 00:00 UT
        semimajor_axis (float): Semimajor axis of the orbit.
        eccentricity (float): eccentricity
        inclination (float): inclination (deg)
        raan (float): R.A. of ascending node (deg)
        argument_of_periapsis (float): argument of perigee (deg)
        true_anomaly (float): True anomaly in deg.
        bstar (float): drag coefficient (1/earth radii)
    Returns:
        str: line1
        str: line2
    """
    mean_anomaly = calculate_mean_anomaly(radians(true_anomaly % 360),
                                          eccentricity)
    mean_motion = calculate_mean_motion(mean_anomaly, semi_major_axis)
    sat = Satrec()
    sat.sgp4init(WGS72, 'i', satellite_number, epoch, bstar, 0.0, 0.0,
                 eccentricity, radians(argument_of_periapsis),
                 radians(inclination), mean_anomaly, mean_motion,
                 radians(raan))
    sat.__setattr__('revnum', 1)
    # Return the TLE string
    return sat


def create_tle(sat):
    return exporter.export_tle(sat)
